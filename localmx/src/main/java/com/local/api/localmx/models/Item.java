package com.local.api.localmx.models;

import java.util.List; 
public class Item{
    public int id;
    public String name;
    public double total_item_price;
    public double price;
    public int quantity;
    public String instructions;
    public String type;
    public int type_id;
    public int tax_rate;
    public int tax_value;
    public Object parent_id;
    public int item_discount;
    public int cart_discount_rate;
    public int cart_discount;
    public String tax_type;
    public List<Option> options;
}
