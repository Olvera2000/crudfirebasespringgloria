package com.local.api.localmx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.time.format.DateTimeFormatter;

import com.google.gson.Gson;
import com.local.api.localmx.models.Root;


@Component
public class ScheduledTasks {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Scheduled(fixedRate = 2000)
    public void scheduleTaskWithFixedRate() {
        Gson gson = new Gson();    
        final String uri = "https://pos.globalfoodsoft.com/pos/order/pop";
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();        
        headers.set("Content-Type", "application/json");
        headers.set("Authorization", "blqrkf83WC0ljPZ769");
        headers.set("Accept", "application/json");
        headers.set("Glf-Api-Version", "2");
        headers.set("Access-Control-Allow-Origin", "*");
        headers.set("Access-Control-Allow-Credentials", "true");
        
        HttpEntity<String> entity = new HttpEntity<String>("{}", headers);
         
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
        //Este es el string que se recibe y se pasa al objeto        
        Root root = gson.fromJson(response.getBody(), Root.class);
        
        logger.info("Ejecutado - {}", response.getBody());
        //TODO Guardar el valor root en FIREBASE, esta en una lista
    }  
}
