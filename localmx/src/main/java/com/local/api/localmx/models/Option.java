package com.local.api.localmx.models;

public class Option{
    public int id;
    public String name;
    public int price;
    public String group_name;
    public int quantity;
    public String type;
    public int type_id;
}
