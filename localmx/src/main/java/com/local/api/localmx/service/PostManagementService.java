package com.local.api.localmx.service;

import com.local.api.localmx.dto.PostDTO;

import java.util.List;

public interface PostManagementService {
List<PostDTO> list();

  Boolean add(PostDTO post);

  Boolean edit(String id,PostDTO post);

  Boolean delete(String id);
}
